package com.project.farm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Apiary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String location;
    private String description;
    private String owner;

    @ManyToOne
    private User user;

    @OneToMany
    @JoinColumn(name = "apiary_id")
    private List<Hike> hikeList;

    @OneToMany
    @JoinColumn(name = "apiary_id")
    private List<Equipment> equipmentList;

    @OneToMany
    @JoinColumn(name = "apiary_id")
    private List<Event> eventList;

    @OneToMany
    @JoinColumn(name = "apiary_id")
    private List<Florescence> florescenceList;

    @OneToMany
    @JoinColumn(name = "apiary_id")
    private List<Harvest> harvestList;

    @OneToMany
    @JoinColumn(name = "apiary_id")
    private List<Finances> financesList;

    @OneToMany
    @JoinColumn(name = "apiary_id")
    private List<Beehive> beeHive;
}
