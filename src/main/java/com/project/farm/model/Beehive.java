package com.project.farm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Beehive {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private int numberFrames;
    private String type;
    private String mother;
    private String origin;
    private String description;
    private LocalDate additionDate;

    @ManyToOne
    private Apiary apiary;

    @OneToMany
    @JoinColumn(name = "beehive_id")
    private List<Treatment> treatmentList;

    @OneToMany
    @JoinColumn(name = "beehive_id")
    private List<Feeding> feedingList;

    @OneToMany
    @JoinColumn(name = "beehive_id")
    private List<Control> controlList;
}
