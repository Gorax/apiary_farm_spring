package com.project.farm.repository;

import com.project.farm.model.Apiary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiaryRepository extends CrudRepository<Apiary, Integer> {
}
