package com.project.farm.repository;

import com.project.farm.model.Task;
import com.project.farm.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, Integer> {
    List<Task> findAllByUser(User user);

    List<Task> findByTopic(String topic);

    List<Task>findByStartDate(LocalDate startDate);
}
