package com.project.farm.repository;

import com.project.farm.model.Apiary;
import com.project.farm.model.Equipment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EquipmentRepository extends CrudRepository<Equipment, Integer> {
    List<Equipment> findAllByApiary(Apiary apiary);
}
