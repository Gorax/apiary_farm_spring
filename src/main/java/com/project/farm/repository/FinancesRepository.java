package com.project.farm.repository;

import com.project.farm.model.Apiary;
import com.project.farm.model.Finances;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FinancesRepository extends CrudRepository<Finances, Integer> {
    List<Finances> findAllByApiary(Apiary apiary);
    List<Finances> findAll();
}
