package com.project.farm.repository;

import com.project.farm.model.Apiary;
import com.project.farm.model.Harvest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HarvestRepository extends CrudRepository<Harvest, Integer> {
    List<Harvest> findAllByApiary(Apiary apiary);
}
