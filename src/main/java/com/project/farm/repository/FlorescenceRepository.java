package com.project.farm.repository;

import com.project.farm.model.Apiary;
import com.project.farm.model.Florescence;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FlorescenceRepository extends CrudRepository<Florescence, Integer> {
    List<Florescence> findAllByApiary(Apiary apiary);

    List<Florescence> findAll();

    List<Florescence> findByStartDate(LocalDate startDate);
}
