package com.project.farm.repository;

import com.project.farm.model.Apiary;
import com.project.farm.model.Hike;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HikeRepository extends CrudRepository<Hike, Integer> {
    List<Hike> findAllByApiary(Apiary apiary);
}
