package com.project.farm.repository;

import com.project.farm.model.Apiary;
import com.project.farm.model.Beehive;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BeehiveRepository extends CrudRepository<Beehive, Integer> {
    List<Beehive> findAllByApiary(Apiary apiary);
    List<Beehive> findAll();
}
