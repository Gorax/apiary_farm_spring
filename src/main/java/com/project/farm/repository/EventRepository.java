package com.project.farm.repository;

import com.project.farm.model.Apiary;
import com.project.farm.model.Event;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<Event, Integer> {
    List<Event> findAllByApiary(Apiary apiary);

    List<Event> findAll();

    List<Event> findByStartDate(LocalDate startDate);
}
