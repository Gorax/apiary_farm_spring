package com.project.farm.repository;

import com.project.farm.model.Beehive;
import com.project.farm.model.Control;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ControlRepository extends CrudRepository<Control, Integer> {
    List<Control> findAllByBeehive(Beehive beehive);
    List<Control> findAll();
}
