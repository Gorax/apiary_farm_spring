package com.project.farm.repository;

import com.project.farm.model.Beehive;
import com.project.farm.model.Treatment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TreatmentRepository extends CrudRepository<Treatment, Integer> {
    List<Treatment> findAllByBeehive(Beehive beehive);
}
