package com.project.farm.repository;

import com.project.farm.model.Beehive;
import com.project.farm.model.Feeding;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeedingRepository extends CrudRepository<Feeding, Integer> {
    List<Feeding> findAllByBeehive(Beehive beehive);
}
