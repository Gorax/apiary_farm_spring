package com.project.farm.view;

import com.project.farm.model.Finances;
import com.project.farm.repository.FinancesRepository;
import com.project.farm.service.FinancesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FinancesViewController {


    @Autowired
    private FinancesService financesService;

    @Autowired
    private FinancesRepository financesRepository;

    @GetMapping("incomeFinance")
    public ModelAndView incomeView(Model model, Integer apiaryId) {
        setIncomeView(model, apiaryId);
        return new ModelAndView("incomeFinance");
    }

    @GetMapping("lossFinance")
    public ModelAndView lossView(Model model, Integer apiaryId) {
        setLossView(model, apiaryId);
        return new ModelAndView("lossFinance");
    }

    @GetMapping("editFinance")
    public ModelAndView editFinance(Model model, Integer apiaryId, Integer financeId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("financeId", financeId);
        editView(financeId, model);
        return new ModelAndView("editFinance");
    }

    @GetMapping("addNewFinances")
    public ModelAndView addNewFinanceView(Model model, Integer apiaryId) {
        model.addAttribute("apiaryId", apiaryId);
        return new ModelAndView("addNewFinances");
    }

    @PostMapping("addNewFinances")
    public String addNewFinance(String transactionDate, String type, int amount, String description, Integer apiaryId, Model model) {
        financesService.addNewFinances(transactionDate, type, amount, description, apiaryId);
        model.addAttribute("finances", financesService.getIncome(apiaryId));
        model.addAttribute("apiaryId", apiaryId);
        return "incomeFinance";
    }

    @PostMapping("editFinance")
    public String editFinance(String transactionDate, String type, int amount, String description, Integer financeId, Integer apiaryId, Model model) {
        financesService.editFinance(transactionDate, type, amount, description, financeId);
        incomeView(model, apiaryId);
        return "incomeFinance";
    }

    @PostMapping("removeFinance")
    public String removeFinance(Model model, Integer apiaryId, Integer financeId) {
        financesService.removeFinance(financeId);
        setIncomeView(model, apiaryId);
        return "incomeFinance";
    }

    private void setIncomeView(Model model, Integer apiaryId) {
        model.addAttribute("finances", financesService.getIncome(apiaryId));
        model.addAttribute("apiaryId", apiaryId);
    }

    private void setLossView(Model model, Integer apiaryId) {
        model.addAttribute("finances", financesService.getLoss(apiaryId));
        model.addAttribute("apiaryId", apiaryId);
    }

    private void editView(Integer financeId, Model model) {
        Finances finance = financesRepository.findById(financeId).get();
        model.addAttribute("financeId", finance.getId());
        model.addAttribute("transactionDate", finance.getTransactionDate());
        model.addAttribute("type", finance.getType());
        model.addAttribute("amount", finance.getAmount());
        model.addAttribute("description", finance.getDescription());
    }
}
