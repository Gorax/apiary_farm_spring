package com.project.farm.view;

import com.project.farm.model.Hike;
import com.project.farm.repository.HikeRepository;
import com.project.farm.service.HikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HikeViewController {

    @Autowired
    private HikeService hikeService;

    @Autowired
    private HikeRepository hikeRepository;

    @GetMapping("hikes")
    public ModelAndView hikesView(Model model, Integer apiaryId) {
        basicView(model, apiaryId);
        return new ModelAndView("hikes");
    }

    @GetMapping("addNewHike")
    public ModelAndView addNewHikeView(Model model, Integer apiaryId) {
        model.addAttribute("apiaryId", apiaryId);
        return new ModelAndView("addNewHike");
    }

    @GetMapping("editHike")
    public ModelAndView editHike(Model model, Integer apiaryId, Integer hikeId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("hikeId", hikeId);
        editView(hikeId, model);
        return new ModelAndView("editHike");
    }

    @PostMapping("addNewHike")
    public String addNewHike(String startDate, String endDate, String location, String utility, Integer apiaryId, Model model) {
        hikeService.addNewHike(startDate, endDate, location, utility, apiaryId);
        basicView(model, apiaryId);
        return "hikes";
    }

    @PostMapping("editHike")
    public String editHike(String startDate, String endDate, String location, String utility, Integer hikeId, Integer apiaryId, Model model) {
        hikeService.editHike(startDate, endDate, location, utility, hikeId);
        basicView(model, apiaryId);
        return "hikes";
    }

    @PostMapping("removeHike")
    public String removeHike(Model model, Integer apiaryId, Integer hikeId) {
        hikeService.removeHike(hikeId);
        basicView(model, apiaryId);
        return "hikes";
    }

    private void basicView(Model model, Integer apiaryId) {
        model.addAttribute("hikes", hikeService.hikeList(apiaryId));
        model.addAttribute("apiaryId", apiaryId);
    }

    private void editView(Integer hikeId, Model model) {
        Hike hike = hikeRepository.findById(hikeId).get();
        model.addAttribute("hikeId", hike.getId());
        model.addAttribute("startDate", hike.getStartDate());
        model.addAttribute("endDate", hike.getEndDate());
        model.addAttribute("location", hike.getLocation());
        model.addAttribute("utility", hike.getUtility());
    }
}
