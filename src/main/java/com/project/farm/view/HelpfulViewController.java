package com.project.farm.view;

import com.project.farm.service.CalendarService;
import com.project.farm.service.EventService;
import com.project.farm.service.FlorescenceService;
import com.project.farm.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;

@Controller
public class HelpfulViewController {

    @Autowired
    private CalendarService calendarService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private FlorescenceService florescenceService;

    @Autowired
    private EventService eventService;

    @GetMapping("calendar")
    public ModelAndView calendarView(Model model) {
        calendarBasicView(model);
        return new ModelAndView("calendar");
    }

    @GetMapping("nextMonth")
    public ModelAndView calendarNextMonth(Model model, String year, String month) {
        setNextMonthView(model, year, month);
        return new ModelAndView("calendar");
    }

    @GetMapping("previouseMonth")
    public ModelAndView calendarPreviouseMonth(Model model, String year, String month) {
        setPreviousMonthView(model, year, month);
        return new ModelAndView("calendar");
    }

    @PostMapping("calendar")
    public ModelAndView findTasks(Model model, String year, String month, String day) {
        setFindTasksView(model, year, month, day);
        return new ModelAndView("calendarTaskList");
    }

    private void changeCalendarView(LocalDate date, Model model, int size) {
        model.addAttribute("year", date.getYear());
        model.addAttribute("month", date.getMonth().getValue());
        model.addAttribute("size", size);
    }

    private void calendarBasicView(Model model) {
        int year = calendarService.getYear();
        int month = calendarService.getMonth();
        int day = calendarService.generateCalendar(month, year);
        model.addAttribute("year", year);
        model.addAttribute("month", month);
        model.addAttribute("size", day);
        model.addAttribute("eventsList", calendarService.checkEvents(day, month, year));
    }

    private void setFindTasksView(Model model, String year, String month, String day) {
        LocalDate date = LocalDate.of(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
        model.addAttribute("date", date);
        model.addAttribute("date2", LocalDate.now().plusDays(2));
        model.addAttribute("taskList", taskService.calendarTask(date));
        model.addAttribute("florescenceList", florescenceService.calendarFlorescence(date));
        model.addAttribute("eventList", eventService.calendarEvent(date));
    }

    private void setNextMonthView(Model model, String year, String month) {
        LocalDate changedDate = LocalDate.of(Integer.valueOf(year), Integer.valueOf(month), 1).plusMonths(1);
        int size = calendarService.generateCalendar(changedDate.getMonth().getValue(), changedDate.getYear());
        changeCalendarView(changedDate, model, size);
        model.addAttribute("eventsList", calendarService.checkEvents(size, changedDate.getMonth().getValue(), changedDate.getYear()));
    }

    private void setPreviousMonthView(Model model, String year, String month) {
        LocalDate changedDate = LocalDate.of(Integer.valueOf(year), Integer.valueOf(month), 1).minusMonths(1);
        int size = calendarService.generateCalendar(changedDate.getMonth().getValue(), changedDate.getYear());
        changeCalendarView(changedDate, model, size);
        model.addAttribute("eventsList", calendarService.checkEvents(size, changedDate.getMonth().getValue(), changedDate.getYear()));
    }
}
