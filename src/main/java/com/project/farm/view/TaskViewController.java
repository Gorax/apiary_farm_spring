package com.project.farm.view;

import com.project.farm.model.Task;
import com.project.farm.repository.TaskRepository;
import com.project.farm.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

@Controller
public class TaskViewController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskRepository taskRepository;

    @GetMapping("taskList")
    public ModelAndView toDoTaskView(HttpServletRequest req, Model model) {
        model.addAttribute("tasks", taskService.taskList(req));
        model.addAttribute("date", LocalDate.now().plusDays(2));
        return new ModelAndView("taskList");
    }

    @GetMapping("addNewTask")
    public ModelAndView addNewTaskView(Model model) {
        return new ModelAndView("addNewTask");
    }

    @GetMapping("openTasks")
    public ModelAndView openTasks(Model model, HttpServletRequest req) {
        model.addAttribute("openTasks", taskService.openTasks(req));
        return new ModelAndView("openTasks");
    }

    @GetMapping("closedTasks")
    public ModelAndView closedTasks(Model model, HttpServletRequest req) {
        model.addAttribute("closedTasks", taskService.closedTasks(req));
        return new ModelAndView("closedTasks");
    }

    @GetMapping("editTask")
    public ModelAndView editTaskView(Integer taskId, Model model) {
        setEditView(taskId, model);
        return new ModelAndView("editTask");
    }

    @PostMapping("addNewTask")
    public String addNewTask(String startDate, String endDate, String topic, String description, HttpServletRequest req) {
        taskService.addNewTask(startDate, endDate, topic, description, req);
        return "redirect:taskList";
    }

    @PostMapping("editTask")
    public String editTask(String startDate, String endDate, String topic, String description, boolean completed, Integer taskId) {
        taskService.editTask(startDate, endDate, topic, description, completed, taskId);
        return "redirect:taskList";
    }

    @PostMapping("removeTask")
    public String removeTask(Integer taskId) {
        taskService.removeTask(taskId);
        return "redirect:taskList";
    }

    @PostMapping("searchTask")
    public ModelAndView searchTask(Model model, String topic) {
        model.addAttribute("taskList", taskService.searchTask(topic));
        return new ModelAndView("searchTask");
    }

    private void setEditView(Integer taskId, Model model) {
        Task task = taskRepository.findById(taskId).get();
        model.addAttribute("startDate", task.getStartDate());
        model.addAttribute("endDate", task.getEndDate());
        model.addAttribute("topic", task.getTopic());
        model.addAttribute("description", task.getDescription());
        model.addAttribute("taskId", task.getId());
    }
}
