package com.project.farm.view;

import com.project.farm.model.Control;
import com.project.farm.repository.ControlRepository;
import com.project.farm.service.ControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControlViewController {

    @Autowired
    private ControlService controlService;

    @Autowired
    private ControlRepository controlRepository;

    @GetMapping("control")
    public ModelAndView controlView(Model model, Integer apiaryId, Integer beehiveId) {
        basicView(model, beehiveId, apiaryId);
        return new ModelAndView("control");
    }

    @GetMapping("addNewControl")
    public ModelAndView addNewControlView(Model model, Integer apiaryId, Integer beehiveId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("beehiveId", beehiveId);
        return new ModelAndView("addNewControl");
    }

    @GetMapping("editControl")
    public ModelAndView controlTreatment(Model model, Integer apiaryId, Integer beehiveId, Integer controlId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("beehiveId", beehiveId);
        model.addAttribute("controlId", controlId);
        editView(controlId, model);
        return new ModelAndView("editControl");
    }

    @PostMapping("addNewControl")
    public String addNewControl(String controlDate, String studySubject, String state, String recommendations, Integer beehiveId, Integer apiaryId, Model model) {
        controlService.addNewControl(controlDate, studySubject, state, recommendations, beehiveId);
        basicView(model, apiaryId, beehiveId);
        return "control";
    }

    @PostMapping("editControl")
    public String editControl(String controlDate, String studySubject, String state, String recommendations, Integer beehiveId, Integer apiaryId, Integer controlId, Model model) {
        controlService.editControl(controlDate, studySubject, state, recommendations, controlId);
        basicView(model, apiaryId, beehiveId);
        return "control";
    }

    @PostMapping("removeControl")
    public String removeControl(Model model, Integer apiaryId, Integer beehiveId, Integer controlId) {
        controlService.removeControl(controlId);
        basicView(model, apiaryId, beehiveId);
        return "control";
    }

    private void basicView(Model model, Integer apiaryId, Integer beehiveId) {
        model.addAttribute("control", controlService.controlList(beehiveId));
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("beehiveId", beehiveId);
    }

    private void editView(Integer controlId, Model model) {
        Control control = controlRepository.findById(controlId).get();
        model.addAttribute("controlId", control.getId());
        model.addAttribute("controlDate", control.getControlDate());
        model.addAttribute("studySubject", control.getStudySubject());
        model.addAttribute("state", control.getState());
        model.addAttribute("recommendations", control.getRecommendations());
    }
}
