package com.project.farm.view;

import com.project.farm.model.Feeding;
import com.project.farm.repository.FeedingRepository;
import com.project.farm.service.FeedingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FeedingViewController {

    @Autowired
    private FeedingService feedingService;

    @Autowired
    private FeedingRepository feedingRepository;

    @GetMapping("feeding")
    public ModelAndView feedingView(Model model, Integer apiaryId, Integer beehiveId) {
        basicView(model, apiaryId, beehiveId);
        return new ModelAndView("feeding");
    }

    @GetMapping("addNewFeeding")
    public ModelAndView addNewFeedingView(Model model, Integer apiaryId, Integer beehiveId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("beehiveId", beehiveId);
        return new ModelAndView("addNewFeeding");
    }

    @GetMapping("editFeeding")
    public ModelAndView editFeeding(Model model, Integer apiaryId, Integer beehiveId, Integer feedingId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("beehiveId", beehiveId);
        model.addAttribute("feedingId", feedingId);
        editView(feedingId, model);
        return new ModelAndView("editFeeding");
    }

    @PostMapping("addNewFeeding")
    public String addNewFeeding(String feedingDate, String foodType, String product, int quantity, String notes, Integer beehiveId, Integer apiaryId, Model model) {
        feedingService.addNewFeeding(feedingDate, foodType, product, quantity, notes, beehiveId);
        basicView(model, apiaryId, beehiveId);
        return "feeding";
    }

    @PostMapping("editFeeding")
    public String editFeeding(String feedingDate, String foodType, String product, int quantity, String notes, Integer beehiveId, Integer apiaryId, Integer feedingId, Model model) {
        feedingService.editFeeding(feedingDate, foodType, product, quantity, notes, feedingId);
        basicView(model, apiaryId, beehiveId);
        return "feeding";
    }

    @PostMapping("removeFeeding")
    public String removeFeeding(Model model, Integer apiaryId, Integer beehiveId, Integer feedingId) {
        feedingService.removeFeeding(feedingId);
        basicView(model, apiaryId, beehiveId);
        return "feeding";
    }

    private void basicView(Model model, Integer apiaryId, Integer beehiveId) {
        model.addAttribute("feeding", feedingService.feedingList(beehiveId));
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("beehiveId", beehiveId);
    }

    private void editView(Integer feedingId, Model model) {
        Feeding feeding = feedingRepository.findById(feedingId).get();
        model.addAttribute("feedingId", feeding.getId());
        model.addAttribute("feedingDate", feeding.getFeedingDate());
        model.addAttribute("foodType", feeding.getFoodType());
        model.addAttribute("product", feeding.getProduct());
        model.addAttribute("quantity", feeding.getQuantity());
        model.addAttribute("notes", feeding.getNotes());
    }
}
