package com.project.farm.view;

import com.project.farm.model.Equipment;
import com.project.farm.repository.EquipmentRepository;
import com.project.farm.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EquipmentViewController {

    @Autowired
    private EquipmentService equipmentService;

    @Autowired
    private EquipmentRepository equipmentRepository;

    @GetMapping("equipments")
    public ModelAndView equipmentView(Model model, Integer apiaryId) {
        basicView(model, apiaryId);
        return new ModelAndView("equipments");
    }

    @GetMapping("addNewEquipment")
    public ModelAndView addNewEquipmentView(Model model, Integer apiaryId) {
        model.addAttribute("apiaryId", apiaryId);
        return new ModelAndView("addNewEquipment");
    }

    @GetMapping("editEquipment")
    public ModelAndView editHike(Model model, Integer apiaryId, Integer equipmentId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("equipmentId", equipmentId);
        editView(equipmentId, model);
        return new ModelAndView();
    }

    @PostMapping("addNewEquipment")
    public String addNewEquipment(String additionDate, String object, int quantity, String notes, Integer apiaryId, Model model) {
        equipmentService.addNewEquipment(additionDate, object, quantity, notes, apiaryId);
        basicView(model, apiaryId);
        return "equipments";
    }

    @PostMapping("editEquipment")
    public String editHike(String additionDate, String object, int quantity, String notes, Integer equipmentId, Integer apiaryId, Model model) {
        equipmentService.editEquipment(additionDate, object, quantity, notes, equipmentId);
        basicView(model, apiaryId);
        return "equipments";
    }

    @PostMapping("removeEquipment")
    public String removeHike(Model model, Integer apiaryId, Integer equipmentId) {
        equipmentService.removeEquipment(equipmentId);
        basicView(model, apiaryId);
        return "equipments";
    }

    private void basicView(Model model, Integer apiaryId) {
        model.addAttribute("equipments", equipmentService.equipmentList(apiaryId));
        model.addAttribute("apiaryId", apiaryId);
    }

    private void editView(Integer equipmentId, Model model) {
        Equipment equipment = equipmentRepository.findById(equipmentId).get();
        model.addAttribute("equipmentId", equipment.getId());
        model.addAttribute("additionDate", equipment.getAdditionDate());
        model.addAttribute("object", equipment.getObject());
        model.addAttribute("quantity", equipment.getQuantity());
        model.addAttribute("notes", equipment.getNotes());
    }
}
