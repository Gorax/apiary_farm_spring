package com.project.farm.view;

import com.project.farm.model.Harvest;
import com.project.farm.repository.HarvestRepository;
import com.project.farm.service.HarvestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HarvestViewController {

    @Autowired
    private HarvestService harvestService;

    @Autowired
    private HarvestRepository harvestRepository;

    @GetMapping("harvest")
    public ModelAndView equipmentView(Model model, Integer apiaryId) {
        basicView(model, apiaryId);
        return new ModelAndView("harvest");
    }

    @GetMapping("addNewHarvest")
    public ModelAndView addNewEquipmentView(Model model, Integer apiaryId) {
        model.addAttribute("apiaryId", apiaryId);
        return new ModelAndView("addNewHarvest");
    }

    @GetMapping("editHarvest")
    public ModelAndView editHike(Model model, Integer apiaryId, Integer harvestId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("harvestId", harvestId);
        editView(harvestId, model);
        return new ModelAndView();
    }

    @PostMapping("addNewHarvest")
    public String addNewEquipment(String harvestDate, String product, int quantity, String unit, String description, Integer apiaryId, Model model) {
        harvestService.addNewHarvest(harvestDate, product, quantity, unit, description, apiaryId);
        basicView(model, apiaryId);
        return "harvest";
    }

    @PostMapping("editHarvest")
    public String editHike(String harvestDate, String product, int quantity, String unit, String description, Integer harvestId, Integer apiaryId, Model model) {
        harvestService.editHarvest(harvestDate, product, quantity, unit, description, harvestId);
        basicView(model, apiaryId);
        return "harvest";
    }

    @PostMapping("removeHarvest")
    public String removeHike(Model model, Integer apiaryId, Integer harvestId) {
        harvestService.removeHarvest(harvestId);
        basicView(model, apiaryId);
        return "harvest";
    }

    private void basicView(Model model, Integer apiaryId) {
        model.addAttribute("harvest", harvestService.harvestList(apiaryId));
        model.addAttribute("apiaryId", apiaryId);
    }

    private void editView(Integer harvestId, Model model) {
        Harvest harvest = harvestRepository.findById(harvestId).get();
        model.addAttribute("harvestId", harvest.getId());
        model.addAttribute("harvestDate", harvest.getHarvestDate());
        model.addAttribute("product", harvest.getProduct());
        model.addAttribute("quantity", harvest.getQuantity());
        model.addAttribute("unit", harvest.getUnit());
        model.addAttribute("description", harvest.getDescription());
    }
}
