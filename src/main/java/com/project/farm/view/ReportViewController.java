package com.project.farm.view;

import com.project.farm.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ReportViewController {

    @Autowired
    private ReportService reportService;

    @GetMapping("report")
    public ModelAndView reportView(Model model) {
        model.addAttribute("report", reportService.generateReport());
        return new ModelAndView("report");
    }

    @GetMapping("allReports")
    public ModelAndView allReports(Model model) {
        model.addAttribute("allReports", reportService.allReports());
        return new ModelAndView("allReports");
    }

    @PostMapping("removeReport")
    public String removeReport(Integer reportId){
        reportService.removeReport(reportId);
        return "redirect:allReports";
    }
}
