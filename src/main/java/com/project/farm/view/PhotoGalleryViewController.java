package com.project.farm.view;

import com.project.farm.service.PhotoGalleryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PhotoGalleryViewController {

    @Autowired
    private PhotoGalleryService photoGalleryService;

    @GetMapping("photoGallery")
    public ModelAndView photoGalleryView(Model model) {
        model.addAttribute("photos", photoGalleryService.getPhotoList());
        return new ModelAndView("photoGallery");
    }

    @GetMapping("addNewPhoto")
    public ModelAndView addNewPhotoView() {
        return new ModelAndView("addNewPhoto");
    }

    @PostMapping("addNewPhoto")
    public String addNewPhoto(String link) {
        photoGalleryService.addNewPhoto(link);
        return "redirect:photoGallery";
    }

    @PostMapping("removePhoto")
    public String removePhoto(String photoLink) {
        photoGalleryService.removePhoto(photoLink);
        return "redirect:photoGallery";
    }
}
