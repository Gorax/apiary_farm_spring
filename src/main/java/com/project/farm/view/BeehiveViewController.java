package com.project.farm.view;

import com.project.farm.model.Beehive;
import com.project.farm.repository.BeehiveRepository;
import com.project.farm.service.BeehiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BeehiveViewController {

    @Autowired
    private BeehiveService beeHiveService;

    @Autowired
    private BeehiveRepository beeHiveRepository;

    @GetMapping("beehive")
    public ModelAndView beehiveView(Model model, Integer apiaryId) {
        basicView(model, apiaryId);
        return new ModelAndView("beehive");
    }

    @GetMapping("beehiveContent")
    public ModelAndView beehiveContent(Model model, Integer beehiveId, Integer apiaryId){
        model.addAttribute("beehive", beeHiveRepository.findById(beehiveId).get());
        model.addAttribute("apiaryId", apiaryId);
        return new ModelAndView("beehiveContent");
    }

    @GetMapping("addNewBeehive")
    public ModelAndView addNewBeehiveView(Model model, Integer apiaryId) {
        model.addAttribute("apiaryId", apiaryId);
        return new ModelAndView("addNewBeehive");
    }

    @GetMapping("editBeehive")
    public ModelAndView editBeehive(Model model, Integer apiaryId, Integer beehiveId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("beehiveId", beehiveId);
        editView(beehiveId, model);
        return new ModelAndView("editBeehive");
    }

    @PostMapping("addNewBeehive")
    public String addNewBeehive(int numberFrames, String type, String mother, String origin, String description, String additionDate, Integer apiaryId, Model model) {
        beeHiveService.addNewBeehive(numberFrames, type, mother, origin, description, additionDate, apiaryId);
        basicView(model, apiaryId);
        return "beehive";
    }

    @PostMapping("editBeehive")
    public String editBeehive(int numberFrames, String type, String mother, String origin, String description, String additionDate, Integer beehiveId, Integer apiaryId, Model model) {
        beeHiveService.editBeehive(numberFrames, type, mother, origin, description, additionDate, beehiveId);
        basicView(model, apiaryId);
        return "beehive";
    }

    @PostMapping("removeBeehive")
    public String removeBeehive(Model model, Integer apiaryId, Integer beehiveId) {
        beeHiveService.removeBeehive(beehiveId);
        basicView(model, apiaryId);
        return "beehive";
    }

    private void basicView(Model model, Integer apiaryId) {
        model.addAttribute("beehive", beeHiveService.beehiveList(apiaryId));
        model.addAttribute("apiaryId", apiaryId);
    }

    private void editView(Integer beehiveId, Model model) {
        Beehive beeHive = beeHiveRepository.findById(beehiveId).get();
        model.addAttribute("beehiveId", beeHive.getId());
        model.addAttribute("numberFrames", beeHive.getNumberFrames());
        model.addAttribute("type", beeHive.getType());
        model.addAttribute("mother", beeHive.getMother());
        model.addAttribute("origin", beeHive.getOrigin());
        model.addAttribute("description", beeHive.getDescription());
        model.addAttribute("additionDate", beeHive.getAdditionDate());
    }
}
