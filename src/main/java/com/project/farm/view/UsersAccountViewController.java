package com.project.farm.view;

import com.project.farm.model.User;
import com.project.farm.repository.UserRepository;
import com.project.farm.service.UserAccountEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UsersAccountViewController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserAccountEditService userAccountEditService;

    @GetMapping("usersAccount")
    public ModelAndView usersAccountView(HttpServletRequest req, Model model) {
        setUserData(req, model);
        return new ModelAndView("usersAccount");
    }

    @PostMapping("usersAccount")
    public String usersAccountEdit(String firstName, String lastName, String address, String phoneNumber, String email, String password1, String password2, HttpServletRequest req, Model model) {
        if (userAccountEditService.editData(firstName, lastName, address, phoneNumber, email, password1, password2, req)) {
            success(model);
            setUserData(req, model);
            return "usersAccount";
        }

        invalidData(model);
        setUserData(req, model);
        return "usersAccount";
    }

    private void setUserData(HttpServletRequest req, Model model) {
        String email = (String) req.getSession().getAttribute("email");
        User user = userRepository.findByEmail(email).get();

        model.addAttribute("firstName", user.getFirstName());
        model.addAttribute("lastName", user.getLastName());
        model.addAttribute("address", user.getAddress());
        model.addAttribute("phoneNumber", user.getPhoneNumber());
        model.addAttribute("email", user.getEmail());
        model.addAttribute("password", user.getPassword());
    }

    private void invalidData(Model model) {
        model.addAttribute("messageType", "danger");
        model.addAttribute("message", "Invalid Data");
    }

    private void success(Model model) {
        model.addAttribute("messageType", "success");
        model.addAttribute("message", "Data was updated");
    }
}
