package com.project.farm.view;

import com.project.farm.model.Florescence;
import com.project.farm.repository.FlorescenceRepository;
import com.project.farm.service.FlorescenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FlorescenceViewController {


    @Autowired
    private FlorescenceService florescenceService;

    @Autowired
    private FlorescenceRepository florescenceRepository;

    @GetMapping("florescence")
    public ModelAndView florescenceView(Model model, Integer apiaryId) {
        basicView(model, apiaryId);
        return new ModelAndView("florescence");
    }

    @GetMapping("addNewFlorescence")
    public ModelAndView addNewFlorescenceView(Model model, Integer apiaryId) {
        model.addAttribute("apiaryId", apiaryId);
        return new ModelAndView("addNewFlorescence");
    }

    @GetMapping("editFlorescence")
    public ModelAndView editFlorescence(Model model, Integer apiaryId, Integer florescenceId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("florescenceId", florescenceId);
        editView(florescenceId, model);
        return new ModelAndView("editFlorescence");
    }

    @PostMapping("addNewFlorescence")
    public String addNewFlorescence(String startDate, String endDate, String name, String description, Integer apiaryId, Model model) {
        florescenceService.addNewFlorescence(startDate, endDate, name, description, apiaryId);
        basicView(model, apiaryId);
        return "florescence";
    }

    @PostMapping("editFlorescence")
    public String editFlorescence(String startDate, String endDate, String name, String description, Integer florescenceId, Integer apiaryId, Model model) {
        florescenceService.editFlorescence(startDate, endDate, name, description, florescenceId);
        basicView(model, apiaryId);
        return "florescence";
    }

    @PostMapping("removeFlorescence")
    public String removeEvent(Model model, Integer florescenceId, Integer apiaryId) {
        florescenceService.removeFlorescence(florescenceId);
        basicView(model, apiaryId);
        return "florescence";
    }


    private void basicView(Model model, Integer apiaryId) {
        model.addAttribute("florescence", florescenceService.florescenceList(apiaryId));
        model.addAttribute("apiaryId", apiaryId);
    }

    private void editView(Integer florescenceId, Model model) {
        Florescence florescence = florescenceRepository.findById(florescenceId).get();
        model.addAttribute("florescenceId", florescence.getId());
        model.addAttribute("startDate", florescence.getStartDate());
        model.addAttribute("endDate", florescence.getEndDate());
        model.addAttribute("name", florescence.getName());
        model.addAttribute("description", florescence.getDescription());
    }


}
