package com.project.farm.view;

import com.project.farm.model.Event;
import com.project.farm.repository.EventRepository;
import com.project.farm.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EventViewController {

    @Autowired
    private EventService eventService;

    @Autowired
    private EventRepository eventRepository;

    @GetMapping("events")
    public ModelAndView eventView(Model model, Integer apiaryId) {
        basicView(model, apiaryId);
        return new ModelAndView("events");
    }

    @GetMapping("addNewEvents")
    public ModelAndView addNewEventView(Model model, Integer apiaryId) {
        model.addAttribute("apiaryId", apiaryId);
        return new ModelAndView("addNewEvent");
    }

    @GetMapping("editEvent")
    public ModelAndView editEvent(Model model, Integer apiaryId, Integer eventId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("eventId", eventId);
        editView(eventId, model);
        return new ModelAndView("editEvent");
    }

    @PostMapping("addNewEvents")
    public String addNewEvent(String startDate, String endDate, String name, String location, String description, Integer apiaryId, Model model) {
        eventService.addNewEvent(startDate, endDate, name, location, description, apiaryId);
        basicView(model, apiaryId);
        return "events";
    }

    @PostMapping("editEvent")
    public String editEvent(String startDate, String endDate, String name, String location, String description, Integer eventId, Integer apiaryId, Model model) {
        eventService.editEvent(startDate, endDate, name, location, description, eventId);
        basicView(model, apiaryId);
        return "events";
    }

    @PostMapping("removeEvent")
    public String removeEvent(Model model, Integer eventId, Integer apiaryId) {
        eventService.removeEvent(eventId);
        basicView(model, apiaryId);
        return "events";
    }

    private void basicView(Model model, Integer apiaryId) {
        model.addAttribute("events", eventService.eventList(apiaryId));
        model.addAttribute("apiaryId", apiaryId);
    }

    private void editView(Integer eventId, Model model) {
        Event event = eventRepository.findById(eventId).get();
        model.addAttribute("equipmentId", event.getId());
        model.addAttribute("startDate", event.getStartDate());
        model.addAttribute("endDate", event.getEndDate());
        model.addAttribute("name", event.getName());
        model.addAttribute("location", event.getLocation());
        model.addAttribute("description", event.getDescription());
    }
}