package com.project.farm.view;

import com.project.farm.service.ApiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeViewController {

    @Autowired
    private ApiaryService apiaryService;

    @GetMapping("home")
    public ModelAndView homeView(HttpServletRequest req, Model model) {
        model.addAttribute("apiaryList", apiaryService.userApiaryList(req));
        return new ModelAndView("home");
    }
}
