package com.project.farm.view;

import com.project.farm.model.Treatment;
import com.project.farm.repository.TreatmentRepository;
import com.project.farm.service.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TreatmentViewController {

    @Autowired
    private TreatmentService treatmentService;

    @Autowired
    private TreatmentRepository treatmentRepository;

    @GetMapping("treatment")
    public ModelAndView treatmentView(Model model, Integer apiaryId, Integer beehiveId) {
        basicView(model, beehiveId, apiaryId);
        return new ModelAndView("treatment");
    }

    @GetMapping("addNewTreatment")
    public ModelAndView addNewTreatmentView(Model model, Integer apiaryId, Integer beehiveId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("beehiveId", beehiveId);
        return new ModelAndView("addNewTreatment");
    }

    @GetMapping("editTreatment")
    public ModelAndView editTreatment(Model model, Integer apiaryId, Integer beehiveId, Integer treatmentId) {
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("beehiveId", beehiveId);
        model.addAttribute("treatmentId", treatmentId);
        editView(treatmentId, model);
        return new ModelAndView("editTreatment");
    }

    @PostMapping("addNewTreatment")
    public String addNewTreatment(String treatmentDate, String disease, String drug, int amountDrug, String description, Integer beehiveId, Integer apiaryId, Model model) {
        treatmentService.addNewTreatment(treatmentDate, disease, drug, amountDrug, description, beehiveId);
        basicView(model, apiaryId, beehiveId);
        return "treatment";
    }

    @PostMapping("editTreatment")
    public String editTreatment(String treatmentDate, String disease, String drug, int amountDrug, String description, Integer beehiveId, Integer apiaryId, Integer treatmentId, Model model) {
        treatmentService.editTreatment(treatmentDate, disease, drug, amountDrug, description, treatmentId);
        basicView(model, apiaryId, beehiveId);
        return "treatment";
    }

    @PostMapping("removeTreatment")
    public String removeTreatment(Model model, Integer apiaryId, Integer beehiveId, Integer treatmentId) {
        treatmentService.removeTreatment(treatmentId);
        basicView(model, apiaryId, beehiveId);
        return "treatment";
    }

    private void basicView(Model model, Integer apiaryId, Integer beehiveId) {
        model.addAttribute("treatment", treatmentService.treatmentList(beehiveId));
        model.addAttribute("apiaryId", apiaryId);
        model.addAttribute("beehiveId", beehiveId);
    }

    private void editView(Integer treatmentId, Model model) {
        Treatment treatment = treatmentRepository.findById(treatmentId).get();
        model.addAttribute("treatmentId", treatment.getId());
        model.addAttribute("treatmentDate", treatment.getTreatmentDate());
        model.addAttribute("disease", treatment.getDisease());
        model.addAttribute("drug", treatment.getDrug());
        model.addAttribute("amountDrug", treatment.getAmountDrug());
        model.addAttribute("description", treatment.getDescription());
    }
}
