package com.project.farm.view;

import com.project.farm.model.Apiary;
import com.project.farm.repository.ApiaryRepository;
import com.project.farm.service.ApiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ApiaryViewController {

    @Autowired
    private ApiaryService apiaryService;

    @Autowired
    private ApiaryRepository apiaryRepository;

    @GetMapping("apiary")
    public ModelAndView apiaryView(Model model, Integer apiaryId) {
        model.addAttribute("apiary", apiaryRepository.findById(apiaryId).get());
        return new ModelAndView("apiary");
    }

    @GetMapping("addNewApiary")
    public ModelAndView addNewApiaryView() {
        return new ModelAndView("addNewApiary");
    }

    @GetMapping("editApiary")
    public String editApiaryView(Integer apiaryId, Model model) {
        editView(apiaryId, model);
        return "editApiary";
    }

    @PostMapping("addNewApiary")
    public String addNewApiary(String apiaryName, String location, String description, String owner, HttpServletRequest req) {
        apiaryService.addNewApiary(apiaryName, location, description, owner, req);
        return "redirect:home";
    }

    @PostMapping("editApiary")
    public String editApiary(String name, String location, String description, String owner, Integer apiaryId, Model model) {
        apiaryService.editApiary(name, location, description, owner, apiaryId);
        basicView(model, apiaryId);
        return "apiary";
    }

    @PostMapping("removeApiary")
    public String removeApiary(Integer apiaryId) {
        apiaryService.removeApiary(apiaryId);
        return "redirect:home";
    }

    private void basicView(Model model, Integer apiaryId) {
        model.addAttribute("apiary", apiaryRepository.findById(apiaryId).get());
    }

    private void editView(Integer apiaryId, Model model) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();
        model.addAttribute("apiary", apiary);
    }

}

