package com.project.farm.service;

import com.project.farm.model.Apiary;
import com.project.farm.model.Finances;
import com.project.farm.repository.ApiaryRepository;
import com.project.farm.repository.FinancesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class FinancesService {

    @Autowired
    private FinancesRepository financesRepository;

    @Autowired
    private ApiaryRepository apiaryRepository;

    public List<Finances> financesList(Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();
        return financesRepository.findAllByApiary(apiary);
    }

    public void addNewFinances(String transactionDate, String type, int amount, String description, Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();

        Finances finances = new Finances(null, LocalDate.parse(transactionDate).plusDays(1), type, amount, description, apiary);
        financesRepository.save(finances);

        apiary.getFinancesList().add(finances);
        apiaryRepository.save(apiary);
    }

    public List<Finances> getIncome(Integer apiaryId) {
        List<Finances> income = new ArrayList<>();
        for (Finances element : financesList(apiaryId)) {
            if (element.getAmount() > 0)
                income.add(element);
        }
        return income;
    }

    public List<Finances> getLoss(Integer apiaryId) {
        List<Finances> loss = new ArrayList<>();
        for (Finances element : financesList(apiaryId)) {
            if (element.getAmount() <= 0)
                loss.add(element);
        }
        return loss;
    }

    public void removeFinance(Integer financeId) {
        financesRepository.deleteById(financeId);
    }

    public void editFinance(String transactionDate, String type, int amount, String description, Integer financeId) {
        Finances finance = financesRepository.findById(financeId).get();

        finance.setTransactionDate(LocalDate.parse(transactionDate).plusDays(1));
        finance.setType(type);
        finance.setAmount(amount);
        finance.setDescription(description);
        financesRepository.save(finance);
    }
}
