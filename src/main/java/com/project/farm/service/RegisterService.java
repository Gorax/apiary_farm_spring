package com.project.farm.service;

import com.project.farm.model.User;
import com.project.farm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class RegisterService {

    @Autowired
    private UserRepository userRepository;

    public boolean register(String firstName, String lastName, String address, String phoneNumber, String email, String password1, String password2) {
        if (checkForExist(email) && confirmPassword(password1, password2)) {
            userRepository.save(new User(null, firstName, lastName, address, phoneNumber, email, password1, new ArrayList<>(), new ArrayList<>()));
            return true;
        }
        return false;
    }

    public boolean confirmPassword(String password, String password2) {
        return password.equals(password2);
    }

    public boolean checkForExist(String email) {
        return !userRepository.findByEmail(email).isPresent();
    }
}
