package com.project.farm.service;

import com.project.farm.model.User;
import com.project.farm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserAccountEditService {

    @Autowired
    private RegisterService registerService;

    @Autowired
    private UserRepository userRepository;

    public boolean editData(String firstName, String lastName, String address, String phoneNumber, String email, String password1, String password2, HttpServletRequest req) {
        if (confirmEmail(email, req) || registerService.checkForExist(email))
            if (registerService.confirmPassword(password1, password2)) {
                String userEmail = (String) req.getSession().getAttribute("email");
                User user = userRepository.findByEmail(userEmail).get();

                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setAddress(address);
                user.setPhoneNumber(phoneNumber);
                user.setEmail(email);
                user.setPassword(password1);

                userRepository.save(user);
                return true;
            }
        return false;
    }

    private boolean confirmEmail(String email, HttpServletRequest req) {
        String sessionEmail = (String) req.getSession().getAttribute("email");
        return sessionEmail.equals(email);
    }
}
