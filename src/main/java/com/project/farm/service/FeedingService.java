package com.project.farm.service;

import com.project.farm.model.Beehive;
import com.project.farm.model.Feeding;
import com.project.farm.repository.BeehiveRepository;
import com.project.farm.repository.FeedingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class FeedingService {

    @Autowired
    private FeedingRepository feedingRepository;

    @Autowired
    private BeehiveRepository beehiveRepository;

    public List<Feeding> feedingList(Integer beehiveId) {
        Beehive beehive = beehiveRepository.findById(beehiveId).get();
        return feedingRepository.findAllByBeehive(beehive);
    }

    public void addNewFeeding(String feedingDate, String foodType, String product, int quantity, String notes, Integer beehiveId) {
        Beehive beehive = beehiveRepository.findById(beehiveId).get();

        Feeding feeding = new Feeding(null, LocalDate.parse(feedingDate).plusDays(1), foodType, product, quantity, notes, beehive);
        feedingRepository.save(feeding);

        beehive.getFeedingList().add(feeding);
        beehiveRepository.save(beehive);
    }

    public void editFeeding(String feedingDate, String foodType, String product, int quantity, String notes, Integer feedingId) {
        Feeding feeding = feedingRepository.findById(feedingId).get();

        feeding.setFeedingDate(LocalDate.parse(feedingDate).plusDays(1));
        feeding.setFoodType(foodType);
        feeding.setProduct(product);
        feeding.setQuantity(quantity);
        feeding.setNotes(notes);
        feedingRepository.save(feeding);
    }

    public void removeFeeding(Integer feedingId) {
        feedingRepository.deleteById(feedingId);
    }
}
