package com.project.farm.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhotoGalleryService {

    private List<String> photoList = new ArrayList<>();

    public List<String> getPhotoList() {
        return photoList;
    }

    public void addNewPhoto(String link) {
        photoList.add(link);
    }

    public void removePhoto(String photoLink) {
        photoList.remove(photoLink);
    }
}
