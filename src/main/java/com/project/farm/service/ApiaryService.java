package com.project.farm.service;

import com.project.farm.model.Apiary;
import com.project.farm.model.User;
import com.project.farm.repository.ApiaryRepository;
import com.project.farm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class ApiaryService {

    @Autowired
    private LoginService loginService;

    @Autowired
    private ApiaryRepository apiaryRepository;

    @Autowired
    private UserRepository userRepository;

    public void addNewApiary(String apiaryName, String location, String description, String owner, HttpServletRequest req) {
        User user = loginService.sessionUser(req);
        Apiary apiary = new Apiary(null, apiaryName, location, description, owner, user, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        user.getApiaryList().add(apiary);

        apiaryRepository.save(apiary);
        userRepository.save(user);
    }

    public List<Apiary> userApiaryList(HttpServletRequest req) {
        User user = loginService.sessionUser(req);
        return user.getApiaryList();
    }

    public void editApiary(String name, String location, String description, String owner, Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();

        apiary.setName(name);
        apiary.setLocation(location);
        apiary.setDescription(description);
        apiary.setOwner(owner);
        apiaryRepository.save(apiary);
    }

    public void removeApiary(Integer apiaryId) {
        apiaryRepository.deleteById(apiaryId);
    }

}