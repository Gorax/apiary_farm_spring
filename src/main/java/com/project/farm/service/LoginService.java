package com.project.farm.service;

import com.project.farm.model.User;
import com.project.farm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class LoginService {

    @Autowired
    private UserRepository userRepository;

    public boolean login(String email, String password, HttpServletRequest req){
        if (userRepository.findByEmail(email).isPresent()){
            User user = userRepository.findByEmail(email).get();

            if (user.getPassword().equals(password)){
                req.getSession().setAttribute("email",user.getEmail());
                return true;
            }
        }
        return false;
    }

    public User sessionUser(HttpServletRequest req) {
        String email = (String) req.getSession().getAttribute("email");
        return userRepository.findByEmail(email).get();
    }
}
