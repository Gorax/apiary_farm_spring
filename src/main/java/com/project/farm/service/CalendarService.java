package com.project.farm.service;

import com.project.farm.repository.EventRepository;
import com.project.farm.repository.FlorescenceRepository;
import com.project.farm.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class CalendarService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private FlorescenceRepository florescenceRepository;

    @Autowired
    private EventRepository eventRepository;

    public int getMonth() {
        return LocalDate.now().getMonth().getValue();
    }

    public int getYear() {
        return LocalDate.now().getYear();
    }

    private int checkLeapYear(int year) {
        int leapYear = 0;

        if (year % 4 == 0 || year % 100 == 0 && year % 400 == 0)
            leapYear = 1;

        return leapYear;
    }

    public int generateCalendar(int month, int year) {
        int leapYear = checkLeapYear(year);
        int size = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                size = 31;
                break;
            case 2:
                switch (leapYear) {
                    case 0:
                        size = 28;
                        break;
                    case 1:
                        size = 29;
                        break;
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                size = 30;
                break;
        }
        return size;
    }

    public List<Boolean> checkEvents(int day, int month, int year) {
        List<Boolean> eventsList = new ArrayList<>();

        for (int i = 1; i <= day; i++) {
            LocalDate date = LocalDate.of(year, month, i).plusDays(1);
            
            if (
                    !taskRepository.findByStartDate(date).isEmpty() ||
                            !florescenceRepository.findByStartDate(date).isEmpty() ||
                            !eventRepository.findByStartDate(date).isEmpty()
            ) {
                eventsList.add(true);
            } else {
                eventsList.add(false);
            }
        }
        return eventsList;
    }
}
