package com.project.farm.service;

import com.project.farm.model.Apiary;
import com.project.farm.model.Equipment;
import com.project.farm.repository.ApiaryRepository;
import com.project.farm.repository.EquipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class EquipmentService {

    @Autowired
    private EquipmentRepository equipmentRepository;

    @Autowired
    private ApiaryRepository apiaryRepository;

    public List<Equipment> equipmentList(Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();
        return equipmentRepository.findAllByApiary(apiary);
    }

    public void addNewEquipment(String additionDate, String object, int quantity, String notes, Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();

        Equipment equipment = new Equipment(null, LocalDate.parse(additionDate).plusDays(1), object, quantity, notes, apiary);
        equipmentRepository.save(equipment);

        apiary.getEquipmentList().add(equipment);
        apiaryRepository.save(apiary);
    }

    public void editEquipment(String additionDate, String object, int quantity, String notes, Integer equipmentId) {
        Equipment equipment = equipmentRepository.findById(equipmentId).get();

        equipment.setAdditionDate(LocalDate.parse(additionDate).plusDays(1));
        equipment.setObject(object);
        equipment.setQuantity(quantity);
        equipment.setNotes(notes);
        equipmentRepository.save(equipment);
    }

    public void removeEquipment(Integer equipmentId) {
        equipmentRepository.deleteById(equipmentId);
    }
}
