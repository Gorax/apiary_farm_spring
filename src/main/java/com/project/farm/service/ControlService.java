package com.project.farm.service;

import com.project.farm.model.Beehive;
import com.project.farm.model.Control;
import com.project.farm.repository.BeehiveRepository;
import com.project.farm.repository.ControlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ControlService {

    @Autowired
    private ControlRepository controlRepository;

    @Autowired
    private BeehiveRepository beehiveRepository;

    public List<Control> controlList(Integer beehiveId) {
        Beehive beehive = beehiveRepository.findById(beehiveId).get();
        return controlRepository.findAllByBeehive(beehive);
    }

    public void addNewControl(String controlDate, String studySubject, String state, String recommendations, Integer beehiveId) {
        Beehive beehive = beehiveRepository.findById(beehiveId).get();

        Control control = new Control(null, LocalDate.parse(controlDate).plusDays(1), studySubject, state, recommendations, beehive);
        controlRepository.save(control);

        beehive.getControlList().add(control);
        beehiveRepository.save(beehive);
    }

    public void editControl(String controlDate, String studySubject, String state, String recommendations, Integer controlId) {
        Control control = controlRepository.findById(controlId).get();

        control.setControlDate(LocalDate.parse(controlDate).plusDays(1));
        control.setStudySubject(studySubject);
        control.setState(state);
        control.setRecommendations(recommendations);
        controlRepository.save(control);
    }

    public void removeControl(Integer controlId) {
        controlRepository.deleteById(controlId);
    }
}
