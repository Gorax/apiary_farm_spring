package com.project.farm.service;

import com.project.farm.model.Apiary;
import com.project.farm.model.Florescence;
import com.project.farm.repository.ApiaryRepository;
import com.project.farm.repository.FlorescenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class FlorescenceService {

    @Autowired
    private FlorescenceRepository florescenceRepository;

    @Autowired
    private ApiaryRepository apiaryRepository;

    public List<Florescence> florescenceList(Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();
        return florescenceRepository.findAllByApiary(apiary);
    }

    public void addNewFlorescence(String startDate, String endDate, String name, String description, Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();

        Florescence florescence = new Florescence(null, LocalDate.parse(startDate).plusDays(1), LocalDate.parse(endDate).plusDays(1), name, description, apiary);
        florescenceRepository.save(florescence);

        apiary.getFlorescenceList().add(florescence);
        apiaryRepository.save(apiary);
    }

    public void editFlorescence(String startDate, String endDate, String name, String description, Integer florescenceId) {
        Florescence florescence = florescenceRepository.findById(florescenceId).get();

        florescence.setStartDate(LocalDate.parse(startDate).plusDays(1));
        florescence.setEndDate(LocalDate.parse(endDate).plusDays(1));
        florescence.setName(name);
        florescence.setDescription(description);
        florescenceRepository.save(florescence);
    }

    public List<Florescence> calendarFlorescence(LocalDate date) {
        return florescenceRepository.findByStartDate(date.plusDays(1));
    }

    public void removeFlorescence(Integer florescenceId) {
        florescenceRepository.deleteById(florescenceId);
    }
}
