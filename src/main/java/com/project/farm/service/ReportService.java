package com.project.farm.service;

import com.project.farm.model.Finances;
import com.project.farm.model.Report;
import com.project.farm.repository.BeehiveRepository;
import com.project.farm.repository.ControlRepository;
import com.project.farm.repository.FinancesRepository;
import com.project.farm.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ReportService {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private BeehiveRepository beehiveRepository;

    @Autowired
    private FinancesRepository financesRepository;

    @Autowired
    private ControlRepository controlRepository;

    public Report generateReport() {
        LocalDate todayDate = LocalDate.now();

        if (reportRepository.findByReportDate(todayDate).isPresent()) {
            return reportRepository.findByReportDate(todayDate).get();
        } else {
            int amountBeehive = beehiveRepository.findAll().size();

            List<Finances> financesList = financesRepository.findAll();
            int income = 0;
            int loss = 0;
            for (Finances element : financesList) {
                if (element.getAmount() > 0) {
                    income += element.getAmount();
                } else {
                    loss += element.getAmount();
                }
            }

            int amountControl = controlRepository.findAll().size();

            Report report = new Report(null, amountBeehive, income, loss, amountControl, todayDate);
            reportRepository.save(report);
            return report;
        }
    }

    public List<Report> allReports() {
        return reportRepository.findAll();
    }

    public void removeReport(Integer reportId) {
        reportRepository.deleteById(reportId);
    }

}
