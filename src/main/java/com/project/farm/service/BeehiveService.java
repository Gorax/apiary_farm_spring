package com.project.farm.service;

import com.project.farm.model.Apiary;
import com.project.farm.model.Beehive;
import com.project.farm.repository.ApiaryRepository;
import com.project.farm.repository.BeehiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class BeehiveService {

    @Autowired
    private BeehiveRepository beeHiveRepository;

    @Autowired
    private ApiaryRepository apiaryRepository;

    public List<Beehive> beehiveList(Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();
        return beeHiveRepository.findAllByApiary(apiary);
    }

    public void addNewBeehive(int numberFrames, String type, String mother, String origin, String description, String additionDate, Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();

        Beehive beehive = new Beehive(null, numberFrames, type, mother, origin, description, LocalDate.parse(additionDate).plusDays(1), apiary, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        beeHiveRepository.save(beehive);

        apiary.getBeeHive().add(beehive);
        apiaryRepository.save(apiary);
    }

    public void editBeehive(int numberFrames, String type, String mother, String origin, String description, String additionDate, Integer beehiveId) {
        Beehive beeHive = beeHiveRepository.findById(beehiveId).get();

        beeHive.setNumberFrames(numberFrames);
        beeHive.setType(type);
        beeHive.setMother(mother);
        beeHive.setOrigin(origin);
        beeHive.setDescription(description);
        beeHive.setAdditionDate(LocalDate.parse(additionDate).plusDays(1));
        beeHiveRepository.save(beeHive);
    }

    public void removeBeehive(Integer beehiveId) {
        beeHiveRepository.deleteById(beehiveId);
    }
}
