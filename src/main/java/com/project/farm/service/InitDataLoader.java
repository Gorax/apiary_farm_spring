package com.project.farm.service;

import com.project.farm.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class InitDataLoader {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ApiaryRepository apiaryRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private HikeRepository hikeRepository;

    @Autowired
    private EquipmentRepository equipmentRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private FlorescenceRepository florescenceRepository;

    @Autowired
    private HarvestRepository harvestRepository;

    @Autowired
    private FinancesRepository financesRepository;

    @Autowired
    private BeehiveRepository beeHiveRepository;

    @Autowired
    private TreatmentRepository treatmentRepository;

    @Autowired
    private FeedingRepository feedingRepository;

    @Autowired
    private ControlRepository controlRepository;

    @Autowired
    private PhotoGalleryService photoGalleryService;

    @PostConstruct
    public void init() {

        List<String> photoList = photoGalleryService.getPhotoList();
        photoList.add("https://cdn.shopify.com/s/files/1/1145/6198/products/image_bd0609b0-176a-4ca5-9788-1159caa7cd13_2048x2048.jpg?v=1484250595");
        photoList.add("https://www.beeculture.com/wp-content/uploads/2017/11/1e.png");
        photoList.add("https://restorelilacway.com/wp-content/uploads/2018/04/Beehive-skep_HomeFarmerUK.jpg");
        photoList.add("https://www.ebeehq.com/wp-content/uploads/2018/06/how-to-start-a-beehive-without-buying-bees-640x330.jpg");

//        Control control = new Control(null, LocalDate.parse("2019-02-27"), "???", "???", "???", null);
//        controlRepository.save(control);
//
//        Feeding feeding = new Feeding(null, LocalDate.parse("2019-06-04"), "???", "???", 10, "???", null);
//        feedingRepository.save(feeding);
//
//        Treatment treatment = new Treatment(null, LocalDate.parse("2019-06-22"), "???", "???", 14, "sadsad", null);
//        treatmentRepository.save(treatment);
//
//        List<Control> controlList = new ArrayList<>();
//        controlList.add(control);
//        List<Feeding> feedingList = new ArrayList<>();
//        feedingList.add(feeding);
//        List<Treatment> treatmentList = new ArrayList<>();
//        treatmentList.add(treatment);
//        Beehive beeHive = new Beehive(null, 150, "????", "Maja", "Podlasie", "asdf", LocalDate.parse("2019-06-10"), null, treatmentList, feedingList, controlList);
//        beeHiveRepository.save(beeHive);
//
//        Finances finances1 = new Finances(null, LocalDate.parse("2019-06-11"), "????", 2_221, "???", null);
//        financesRepository.save(finances1);
//        Finances finances2 = new Finances(null, LocalDate.parse("2019-06-11"), "????", -222, "???", null);
//        financesRepository.save(finances2);
//
//
//        Harvest harvest = new Harvest(null, LocalDate.parse("2019-06-25"), "cos", 25, "tony?", "very good harvest", null);
//        harvestRepository.save(harvest);
//
//        Florescence florescence = new Florescence(null, LocalDate.parse("2019-06-01").plusDays(1), LocalDate.parse("2019-06-05"), "test", "test", null);
//        florescenceRepository.save(florescence);
//
//        Event event = new Event(null, LocalDate.parse("2019-06-01").plusDays(1), LocalDate.parse("2019-06-05"), "test", "?1?1?!", "-.-", null);
//        eventRepository.save(event);
//
//        Equipment equipment = new Equipment(null, LocalDate.parse("2019-06-28"), "hammer", 35, "very good hammers", null);
//        equipmentRepository.save(equipment);
//
//        Hike hike = new Hike(null, LocalDate.parse("2019-06-12"), LocalDate.parse("2019-06-12"), "??????", "???????", null);
//        hikeRepository.save(hike);
//
//        List<Beehive> beehiveList = new ArrayList<>();
//        beehiveList.add(beeHive);
//        List<Finances> financesList = new ArrayList<>();
//        financesList.add(finances1);
//        financesList.add(finances2);
//        List<Harvest> harvestList = new ArrayList<>();
//        harvestList.add(harvest);
//        List<Florescence> florescenceList = new ArrayList<>();
//        florescenceList.add(florescence);
//        List<Event> eventList = new ArrayList<>();
//        eventList.add(event);
//        List<Equipment> equipmentList = new ArrayList<>();
//        equipmentList.add(equipment);
//        List<Hike> hikeList = new ArrayList<>();
//        hikeList.add(hike);
//        Apiary apiary = new Apiary(null, "adminApiary", "dark side", "very good apiary", "admin???", null, hikeList, equipmentList, eventList, florescenceList, harvestList, financesList, beehiveList);
//        apiaryRepository.save(apiary);
//
//        Task task = new Task(null, LocalDate.parse("2019-06-01").plusDays(1), LocalDate.parse("2019-06-18"), "test", "admin test", false, null);
//        taskRepository.save(task);
//
//        List<Apiary> apiaryList = new ArrayList<>();
//        apiaryList.add(apiary);
//        List<Task> taskList = new ArrayList<>();
//        taskList.add(task);
//        userRepository.save(new User(null, "admin", "admin", "dark side 4a", "666-666-666", "admin@admin", "admin", apiaryList, taskList));
    }
}