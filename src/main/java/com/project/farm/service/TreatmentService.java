package com.project.farm.service;

import com.project.farm.model.Beehive;
import com.project.farm.model.Treatment;
import com.project.farm.repository.BeehiveRepository;
import com.project.farm.repository.TreatmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TreatmentService {

    @Autowired
    private TreatmentRepository treatmentRepository;

    @Autowired
    private BeehiveRepository beehiveRepository;

    public List<Treatment> treatmentList(Integer beehiveId) {
        Beehive beehive = beehiveRepository.findById(beehiveId).get();
        return treatmentRepository.findAllByBeehive(beehive);
    }

    public void addNewTreatment(String treatmentDate, String disease, String drug, int amountDrug, String description, Integer beehiveId) {
        Beehive beehive = beehiveRepository.findById(beehiveId).get();

        Treatment treatment = new Treatment(null, LocalDate.parse(treatmentDate).plusDays(1), disease, drug, amountDrug, description, beehive);
        treatmentRepository.save(treatment);

        beehive.getTreatmentList().add(treatment);
        beehiveRepository.save(beehive);
    }

    public void editTreatment(String treatmentDate, String disease, String drug, int amountDrug, String description, Integer treatmentId) {
        Treatment treatment = treatmentRepository.findById(treatmentId).get();

        treatment.setTreatmentDate(LocalDate.parse(treatmentDate).plusDays(1));
        treatment.setDisease(disease);
        treatment.setDrug(drug);
        treatment.setAmountDrug(amountDrug);
        treatment.setDescription(description);
        treatmentRepository.save(treatment);
    }

    public void removeTreatment(Integer treatmentId) {
        treatmentRepository.deleteById(treatmentId);
    }
}
