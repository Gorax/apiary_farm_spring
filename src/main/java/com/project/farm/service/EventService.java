package com.project.farm.service;

import com.project.farm.model.Apiary;
import com.project.farm.model.Event;
import com.project.farm.repository.ApiaryRepository;
import com.project.farm.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private ApiaryRepository apiaryRepository;

    public List<Event> eventList(Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();
        return eventRepository.findAllByApiary(apiary);
    }

    public void addNewEvent(String startDate, String endDate, String name, String location, String description, Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();

        Event event = new Event(null, LocalDate.parse(startDate).plusDays(1), LocalDate.parse(endDate).plusDays(1), name, location, description, apiary);
        eventRepository.save(event);

        apiary.getEventList().add(event);
        apiaryRepository.save(apiary);
    }

    public void editEvent(String startDate, String endDate, String name, String location, String description, Integer eventId) {
        Event event = eventRepository.findById(eventId).get();

        event.setStartDate(LocalDate.parse(startDate).plusDays(1));
        event.setEndDate(LocalDate.parse(endDate).plusDays(1));
        event.setName(name);
        event.setLocation(location);
        event.setDescription(description);
        eventRepository.save(event);
    }

    public List<Event> calendarEvent(LocalDate date) {
        return eventRepository.findByStartDate(date.plusDays(1));
    }

    public void removeEvent(Integer eventId) {
        eventRepository.deleteById(eventId);
    }
}
