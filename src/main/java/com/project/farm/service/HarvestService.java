package com.project.farm.service;

import com.project.farm.model.Apiary;
import com.project.farm.model.Harvest;
import com.project.farm.repository.ApiaryRepository;
import com.project.farm.repository.HarvestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class HarvestService {

    @Autowired
    private HarvestRepository harvestRepository;

    @Autowired
    private ApiaryRepository apiaryRepository;

    public List<Harvest> harvestList(Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();
        return harvestRepository.findAllByApiary(apiary);
    }

    public void addNewHarvest(String harvestDate, String product, int quantity, String unit, String description, Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();

        Harvest harvest = new Harvest(null, LocalDate.parse(harvestDate).plusDays(1), product, quantity, unit, description, apiary);
        harvestRepository.save(harvest);

        apiary.getHarvestList().add(harvest);
        apiaryRepository.save(apiary);
    }

    public void editHarvest(String harvestDate, String product, int quantity, String unit, String description, Integer harvestId) {
        Harvest harvest = harvestRepository.findById(harvestId).get();

        harvest.setHarvestDate(LocalDate.parse(harvestDate).plusDays(1));
        harvest.setProduct(product);
        harvest.setQuantity(quantity);
        harvest.setUnit(unit);
        harvest.setDescription(description);
        harvestRepository.save(harvest);
    }

    public void removeHarvest(Integer harvestId) {
        harvestRepository.deleteById(harvestId);
    }
}
