package com.project.farm.service;

import com.project.farm.model.Apiary;
import com.project.farm.model.Hike;
import com.project.farm.repository.ApiaryRepository;
import com.project.farm.repository.HikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class HikeService {

    @Autowired
    private HikeRepository hikeRepository;

    @Autowired
    private ApiaryRepository apiaryRepository;

    public List<Hike> hikeList(Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();
        return hikeRepository.findAllByApiary(apiary);
    }

    public void addNewHike(String startDate, String endDate, String location, String utility, Integer apiaryId) {
        Apiary apiary = apiaryRepository.findById(apiaryId).get();

        Hike hike = new Hike(null, LocalDate.parse(startDate).plusDays(1), LocalDate.parse(endDate).plusDays(1), location, utility, apiary);
        hikeRepository.save(hike);

        apiary.getHikeList().add(hike);
        apiaryRepository.save(apiary);
    }

    public void editHike(String startDate, String endDate, String location, String utility, Integer hikeId) {
        Hike hike = hikeRepository.findById(hikeId).get();

        hike.setStartDate(LocalDate.parse(startDate).plusDays(1));
        hike.setEndDate(LocalDate.parse(endDate).plusDays(1));
        hike.setLocation(location);
        hike.setUtility(utility);
        hikeRepository.save(hike);
    }

    public void removeHike(Integer hikeId) {
        hikeRepository.deleteById(hikeId);
    }
}
