package com.project.farm.service;

import com.project.farm.model.Task;
import com.project.farm.model.User;
import com.project.farm.repository.TaskRepository;
import com.project.farm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private LoginService loginService;

    @Autowired
    private UserRepository userRepository;

    public List<Task> taskList(HttpServletRequest req) {
        User user = loginService.sessionUser(req);
        List<Task> taskList = taskRepository.findAllByUser(user);
        Collections.sort(taskList);
        return taskList;
    }

    public List<Task> closedTasks(HttpServletRequest req) {
        List<Task> closedTasks = new ArrayList<>();
        for (Task element : taskList(req)) {
            if (element.isCompleted())
                closedTasks.add(element);
        }
        Collections.sort(closedTasks);
        return closedTasks;
    }

    public List<Task> openTasks(HttpServletRequest req) {
        List<Task> openTasks = new ArrayList<>();
        for (Task element : taskList(req)) {
            if (!element.isCompleted())
                openTasks.add(element);
        }
        Collections.sort(openTasks);
        return openTasks;
    }

    public List<Task> searchTask(String topic) {
        return taskRepository.findByTopic(topic);
    }

    public void addNewTask(String startDate, String endDate, String topic, String description, HttpServletRequest req) {
        User user = loginService.sessionUser(req);
        Task task = new Task(null, LocalDate.parse(startDate).plusDays(1), LocalDate.parse(endDate).plusDays(1), topic, description, false, user);
        taskRepository.save(task);
        user.getTaskList().add(task);
        userRepository.save(user);

    }

    public void editTask(String startDate, String endDate, String topic, String description, boolean completed, Integer taskId) {
        Task task = taskRepository.findById(taskId).get();

        task.setStartDate(LocalDate.parse(startDate).plusDays(1));
        task.setEndDate(LocalDate.parse(endDate).plusDays(1));
        task.setTopic(topic);
        task.setDescription(description);
        task.setCompleted(completed);

        taskRepository.save(task);
    }

    public void removeTask(Integer taskId) {
        taskRepository.deleteById(taskId);
    }

    public List<Task> calendarTask(LocalDate date) {
        return taskRepository.findByStartDate(date.plusDays(1));
    }
}
